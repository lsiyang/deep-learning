# -*- coding: utf-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F

from models.CHNL_Block import CHNL_Block
from models.CA_Block import CA_Block
from models.GCBlock import GCBlock
from models.Res2BottleNeck import Res2NetBottleneck
from models.SK_Block import SKBlock
from models.Res_Block import ResBlock


class GC_ResBlock(nn.Module):
    """残差块

    属性:
        expansion: 第二个卷积层中输出通道数对于输入通道数扩展的倍数
    """
    expansion = 1

    """
    初始化

    参数:
        in_channel: 输入通道数
        out_channel: 输出通道数
        stride: 步长
    """

    def __init__(self, in_channel, out_channel, stride=1):
        super(GC_ResBlock, self).__init__()
        # self.is_add_gc = (out_channel >= 256)
        self.layer = nn.Sequential(
            nn.Conv2d(in_channel, out_channel, kernel_size=3, stride=stride, padding=1, bias=False),
            nn.BatchNorm2d(out_channel),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channel, out_channel * self.expansion, kernel_size=3, stride=1, padding=1, bias=True),
            nn.BatchNorm2d(out_channel * self.expansion)
        )
        self.shortcut = nn.Sequential()
        if stride != 1 or in_channel != out_channel * self.expansion:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_channel, out_channel * self.expansion, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(out_channel * self.expansion)
            )
        self.gc_block = GCBlock(out_channel, scale=16)

    def forward(self, x):
        out1 = self.layer(x)
        # add GCBlock in layer3 and layer4's ResBlock
        # if self.is_add_gc:
        out1 = self.gc_block(out1)
        out2 = self.shortcut(x)
        out = out1 + out2
        return out


class CA_ResBlock(nn.Module):
    """残差块

    属性:
        expansion: 第二个卷积层中输出通道数对于输入通道数扩展的倍数
    """
    expansion = 1

    """
    初始化

    参数:
        in_channel: 输入通道数
        out_channel: 输出通道数
        stride: 步长
    """

    def __init__(self, in_channel, out_channel, stride=1):
        super(CA_ResBlock, self).__init__()
        self.layer = nn.Sequential(
            nn.Conv2d(in_channel, out_channel, kernel_size=3, stride=stride, padding=1, bias=False),
            nn.BatchNorm2d(out_channel),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channel, out_channel * self.expansion, kernel_size=3, stride=1, padding=1, bias=True),
            nn.BatchNorm2d(out_channel * self.expansion)
        )
        self.ca = CA_Block(out_channel * self.expansion)
        self.shortcut = nn.Sequential()
        if stride != 1 or in_channel != out_channel * self.expansion:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_channel, out_channel * self.expansion, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(out_channel * self.expansion)
            )

    def forward(self, x):
        out1 = self.layer(x)
        out2 = self.shortcut(x)
        out = out1 + out2
        out = self.ca(out)
        return out


class Compound_ResNet(nn.Module):
    """残差网络
    初始化
    参数：
        ResBlock: 残差块
        num_blocks: 每层基本块个数，为一维数组(两层,两个整数), ResNet10: [2, 2]
        num_classes: 分类数
    """

    def __init__(self, Blocks, num_blocks, strides, num_classes=10):
        self.in_channel = 64
        super(Compound_ResNet, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        layers = []
        for i, block in enumerate(Blocks):
            layers.append(self.make_layer(Blocks[i], 64 * pow(2, i), num_blocks[i], strides[i]))
        self.layers = nn.Sequential(*layers)
        # self.layer1 = self.make_layer(Blocks[0], 64, num_blocks[0], stride=1)
        # self.layer2 = self.make_layer(Blocks[1], 128, num_blocks[1], stride=2)
        # self.layer3 = self.make_layer(Blocks[2], 256, num_blocks[2], stride=2)
        # self.layer4 = self.make_layer(Blocks[3], 512, num_blocks[3], stride=2)
        self.fc = nn.Linear(64 * pow(2, len(Blocks) - 1), num_classes)

    """
    组建网络层

    参数：
        block: 网络模块
        out_channels: 输出通道数
        num_blocks: 模块个数
        stride: 步长
    """

    def make_layer(self, block, out_channels, num_blocks, stride):
        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_channel, out_channels, stride))
            self.in_channel = out_channels * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.conv1(x)
        out = self.layers(out)
        # out = self.layer1(out)
        # out = self.layer2(out)
        # print(out.shape)
        out = F.avg_pool2d(out, out.shape[3])
        out = out.view(out.size(0), -1)
        # print(out.shape)
        out = self.fc(out)
        return out


def ResNet10(num_classes=100):
    return Compound_ResNet([ResBlock, ResBlock], [2, 2], [1, 2], num_classes)


def CANet(num_classes=100):
    return Compound_ResNet([CA_ResBlock, CA_ResBlock], [2, 2], [1, 2], num_classes)


def SKNet(num_classes=100):
    return Compound_ResNet([SKBlock, SKBlock], [2, 2], [1, 2], num_classes)


def GCNet(num_classes=100):
    return Compound_ResNet([GC_ResBlock, GC_ResBlock], [2, 2], [1, 2], num_classes)


def Res2Net(num_classes=100):
    return Compound_ResNet([Res2NetBottleneck, Res2NetBottleneck], [2, 2], [1, 2], num_classes)


def SK_GC_ResNet10(num_classes=100):
    return Compound_ResNet([SKBlock, GC_ResBlock], [2, 2], [1, 2], num_classes)


def CA_GC_ResNet10(num_classes=100):
    return Compound_ResNet([CA_ResBlock, GC_ResBlock], [2, 2], [1, 2], num_classes)


def Base_GC_ResNet10(num_classes=100):
    return Compound_ResNet([ResBlock, GC_ResBlock], [2, 2], [1, 2], num_classes)


def CA_Base_GC_ResNet14(num_classes=100):
    return Compound_ResNet([CA_ResBlock, ResBlock, GC_ResBlock], [2, 2, 2], [1, 2, 2], num_classes)


def SK_Base_GC_ResNet14(num_classes=100):
    return Compound_ResNet([SKBlock, ResBlock, GC_ResBlock], [2, 2, 2], [1, 2, 2], num_classes)

def CA_Res2_GC_ResNet14(num_classes=100):
    return Compound_ResNet([CA_ResBlock, Res2NetBottleneck, GC_ResBlock], [2, 2, 2], [1, 2, 2], num_classes)

def SK_Res2_GC_ResNet14(num_classes=100):
    return Compound_ResNet([SKBlock, Res2NetBottleneck, GC_ResBlock], [2, 2, 2], [1, 2, 2], num_classes)

def CA_CHNL_GC_ResNet14(num_classes=100):
    return Compound_ResNet([CA_ResBlock, CHNL_Block, GC_ResBlock], [2, 2, 2], [1, 2, 2], num_classes)

def Net(num_classes=100):
    # return ResNet10(num_classes), ResNet10.__name__
    # return CANet(num_classes), CANet.__name__
    # return SKNet(num_classes), SKNet.__name__
    # return GCNet(num_classes), GCNet.__name__
    # return Res2Net(num_classes), Res2Net.__name__

    # return SK_GC_ResNet10(num_classes), SK_GC_ResNet10.__name__
    # return CA_GC_ResNet10(num_classes), CA_GC_ResNet10.__name__
    # return Base_GC_ResNet10(num_classes), Base_GC_ResNet10.__name__

    # return CA_Base_GC_ResNet14(num_classes), CA_Base_GC_ResNet14.__name__
    # return CA_Res2_GC_ResNet14(num_classes), CA_Res2_GC_ResNet14.__name__
    # return SK_Base_GC_ResNet14(num_classes), SK_Base_GC_ResNet14.__name__
    # return SK_Res2_GC_ResNet14(num_classes), SK_Res2_GC_ResNet14.__name__

    return CA_CHNL_GC_ResNet14(num_classes), CA_CHNL_GC_ResNet14.__name__


if __name__ == '__main__':
    net, save_name = Net(100)
    # print(net)
    print(save_name)
    x = torch.randn(4, 3, 32, 32)
    out = net(x)
    # print(out.shape)
