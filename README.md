# DeepLearning

#### 介绍
深度学习课程小组大作业，复现论文并实现创新点，完成cifar100分类任务

#### 复现论文
Selective Kernel Networks  
https://doi.org/10.48550/arXiv.1903.06586




Coordinate Attention for Efficient Mobile Network Design
https://doi.org/10.48550/arXiv.2103.02907



GCNet: Non-local Networks Meet Squeeze-Excitation Networks and Beyond
https://doi.org/10.48550/arXiv.1904.11492


Res2Net: A New Multi-scale Backbone Architecture
https://doi.org/10.48550/arXiv.1904.01169